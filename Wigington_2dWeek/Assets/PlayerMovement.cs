﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;
    private int count;
    public SpriteRenderer spriteRenderer;
    public TextMeshProUGUI countText;

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();

        count = 0;

        SetCountText();

    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2 (1f, .1f), 0f, Vector2.down, .01f, levelMask))

            Jump();
        }
    }

   

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
            spriteRenderer.flipX = false;
        else
        if (rB2D.velocity.x < 0)
            spriteRenderer.flipX = true;

        if (Mathf.Abs(horizontalInput) > 0f)
            animator.SetBool("IsRunning", true);
        else
            animator.SetBool("IsRunning", false);
    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
    }
}
